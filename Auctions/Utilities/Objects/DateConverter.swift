//
//  DateConverter.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Foundation

enum DateConversionError: Error {
    case failedConvertingFromString(_ string: String)
}

class DateConverter {
    
    enum DateFormat {
        case ISO8601
        
        func toString() -> String {
            switch self {
            case .ISO8601: return "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            }
        }
    }
    
    private static let shared = DateConverter()
    private let dateFormatter = DateFormatter()
    
    private static func toDate(dateString: String, format: String) throws -> Date {
        let converter = DateConverter.shared
        converter.dateFormatter.dateFormat = format
        guard let date = converter.dateFormatter.date(from: dateString) else {
            throw DateConversionError.failedConvertingFromString(dateString)
        }
        return date
    }
    
    private static func toString(date: Date, format: String?) -> String {
        let converter = DateConverter.shared
        converter.dateFormatter.dateFormat = format
        return converter.dateFormatter.string(from: date)
    }
    
    static func toDate(dateString: String, format: DateFormat) throws -> Date {
        return try toDate(dateString: dateString, format: format.toString())
    }
    
    static func toString(date: Date, format: DateFormat) -> String {
        return toString(date: date, format: format.toString())
    }
}
