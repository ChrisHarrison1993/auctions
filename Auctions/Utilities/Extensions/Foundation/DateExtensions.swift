//
//  DateExtensions.swift
//  Auctions
//
//  Created by Harrison, Chris (UK - London) on 25/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Foundation

extension Date {
    
    func isPast() -> Bool {
        return self <= Date()
    }
}
