//
//  AuctionCell.swift
//  Auctions
//
//  Created by Harrison, Chris (UK - London) on 25/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import UIKit

class AuctionCell: UITableViewCell, AuctionUpdateable {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var countdownLabel: UILabel!
    @IBOutlet var estimatedReturnLabel: UILabel!
    @IBOutlet var showMoreLabel: UILabel!
    
    func update(with auctionField: AuctionField) {
        titleLabel.style(with: auctionField.title)
        countdownLabel.style(with: auctionField.closeTime)
        estimatedReturnLabel.style(with: auctionField.estimatedReturn)
        estimatedReturnLabel.isHidden = !auctionField.showEstimatedReturn
        
        if auctionField.showEstimatedReturn {
            showMoreLabel.style(with: StyledText(text: "Hide estimated return", style: .body, color: showMoreLabel.tintColor))
        } else {
            showMoreLabel.style(with: StyledText(text: "Show estimated return", style: .body, color: showMoreLabel.tintColor))
        }
    }
}
