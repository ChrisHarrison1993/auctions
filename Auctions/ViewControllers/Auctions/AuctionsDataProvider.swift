//
//  AuctionsDataProvider.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import UIKit

class AuctionsDataProvider: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            registerCells()
        }
    }
    
    var viewModel = AuctionsViewModel()
    
    override init() {
        super.init()
        viewModel.delegate = self
    }
    
    func registerCells() {
        tableView.register(AuctionCell.nib(), forCellReuseIdentifier: AuctionCell.className)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.item(at: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AuctionCell.className, for: indexPath) as? AuctionCell else {
            return UITableViewCell()
        }
        cell.update(with: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        viewModel.switchShowEstimatedReturnState(at: indexPath)
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
}

extension AuctionsDataProvider: AuctionsViewModelDelegate {
    
    func items(insertedAtIndexPaths: [IndexPath], deletedAtIndexPaths: [IndexPath], modifiedAtIndexPaths: [IndexPath]) {
        tableView.beginUpdates()
        tableView.insertRows(at: insertedAtIndexPaths, with: .automatic)
        tableView.deleteRows(at: deletedAtIndexPaths, with: .automatic)
        tableView.reloadRows(at: modifiedAtIndexPaths, with: .automatic)
        tableView.endUpdates()
    }
    
    func reload() {
        tableView.reloadData()
    }
}
