//
//  AuctionField.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Foundation

struct AuctionField {
    
    var title: StyledText
    var closeTime: StyledText
    var estimatedReturn: StyledText
    var showEstimatedReturn: Bool = false
    
    init(title: String, closeTime: String, estimatedReturn: String) {
        self.title = StyledText(text: title, style: .title1)
        self.closeTime = StyledText(text: closeTime, style: .body)
        self.estimatedReturn = StyledText(text: estimatedReturn, style: .body)
    }
}
