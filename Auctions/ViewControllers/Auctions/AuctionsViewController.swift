//
//  ViewController.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import UIKit
import RealmSwift

class AuctionsViewController: UIViewController {
    
    @IBOutlet var dataProvider: AuctionsDataProvider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AuctionService.shared.getAllAuctions { (auctions) in
            DBManager.shared.save(auctions)
        }
    }
}

