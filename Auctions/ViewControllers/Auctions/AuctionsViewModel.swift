//
//  AuctionsViewModel.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import RealmSwift

protocol AuctionsViewModelDelegate {
    func items(insertedAtIndexPaths: [IndexPath], deletedAtIndexPaths: [IndexPath], modifiedAtIndexPaths: [IndexPath])
    func reload()
}

class AuctionsViewModel {
    
    var notificationToken: NotificationToken? = nil
    var auctionFields: [AuctionField] = []
    var auctions: Results<Auction>!
    
    var delegate: AuctionsViewModelDelegate?
    
    init() {
        observeAuctions()
        updateFields()
        self.reload()
    }
    
    func numberOfRows(in section: Int) -> Int {
        return auctionFields.count
    }
    
    func item(at indexPath: IndexPath) -> AuctionField {
        return auctionFields[indexPath.row]
    }
    
    func switchShowEstimatedReturnState(at indexPath: IndexPath) {
        auctionFields[indexPath.row].showEstimatedReturn = !auctionFields[indexPath.row].showEstimatedReturn
    }
    
    func observeAuctions() {
        auctions = DBManager.shared.allAuctions()
        notificationToken = auctions.observe { (changes: RealmCollectionChange) in
            self.updateFields()
            switch changes {
            case .initial(_):
                self.reload()
            case .update(_, let deletions, let insertions, let modifications):
                self.delegate?.items(insertedAtIndexPaths: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     deletedAtIndexPaths: deletions.map({ IndexPath(row: $0, section: 0) }),
                                     modifiedAtIndexPaths: modifications.map({ IndexPath(row: $0, section: 0) }))
            case .error(_):
                return
            }
        }
    }
    
    func updateFields() {
        auctionFields = auctions.map({ (auction)  in
            let title = auction.title
            let closeTime = countdownText(to: auction.closeTime)
            let estimatedReturnGBP = estimatedReturnAmount(auctionRate: auction.rate, riskBand: auction.riskBand, bidAmount: auction.amountGBP)
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_GB")
            formatter.numberStyle = .currency
            let formattedEstimatedReturn = formatter.string(from: estimatedReturnGBP as NSNumber)
            return AuctionField(title: title, closeTime: closeTime, estimatedReturn: formattedEstimatedReturn ?? "")
            })
    }
    
    func reload() {
        self.delegate?.reload()
    }
    
    func estimatedReturnAmount(auctionRate: Double, riskBand: String, fee: Double = 0.01, bidAmount: Double = 20) -> Double {
        let estimatedBadDebt = RiskBand(rawValue: riskBand)?.estimatedBadDebt ?? 0
        return (1 + auctionRate - estimatedBadDebt - fee) * bidAmount
    }
    
    func countdownText(to date: Date) -> String {
        let userCalendar = NSCalendar.current
        var countdownText: String!
        if date.isPast() {
            countdownText = "Expired"
        } else {
            let closeTimeComponents = userCalendar.dateComponents([.day, .hour], from: Date(), to: date)
            countdownText = "\(closeTimeComponents.day?.description ?? "0") days \(closeTimeComponents.hour?.description ?? "0") hours remaining"
        }
        return countdownText
    }
}

