//
//  Auction.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class Auction: Object, Decodable {
    
    private enum AuctionCodingKeys: String, CodingKey {
        case id
        case title
        case rate
        case amount_cents
        case term
        case risk_band
        case close_time
    }
    
    dynamic var id: Int = 0
    dynamic var title: String = ""
    dynamic var rate: Double = 0.0
    dynamic var amountGBP: Double = 0.0
    dynamic var term: Int = 0
    dynamic var riskBand: String = ""
    dynamic var closeTime: Date = Date()
    
    //1 cent = 0.0076 GBP as of 25/03/2019
    let exchangeRate: Double = 0.0076
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: AuctionCodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.rate = try container.decode(Double.self, forKey: .rate)
        let amountCents = try container.decode(Double.self, forKey: .amount_cents)
        self.amountGBP = amountCents * exchangeRate
        self.term = try container.decode(Int.self, forKey: .term)
        self.riskBand = try container.decode(String.self, forKey: .risk_band)
        let closeTimeString = try container.decode(String.self, forKey: .close_time)
        self.closeTime = try DateConverter.toDate(dateString: closeTimeString, format: .ISO8601)
        super.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
