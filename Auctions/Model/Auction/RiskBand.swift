//
//  RiskBand.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Foundation

enum RiskBandInitializerError: Error {
    case failedInitializingFromString(_ string: String)
}

enum RiskBand: String {
    case A_Plus = "A+"
    case A = "A"
    case B = "B"
    case C = "C"
    case C_Minus = "C-"
    
    var estimatedBadDebt: Double {
        switch self {
        case .A_Plus: return 0.01
        case .A: return 0.02
        case .B: return 0.03
        case .C: return 0.04
        case .C_Minus: return 0.05
        }
    }
}
