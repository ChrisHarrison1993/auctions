//
//  AuctionService.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Alamofire

struct AuctionService {
    
    static let shared = AuctionService()
    
    func getAllAuctions(completion: @escaping (_ auctions: [Auction]) -> ()) {
        Alamofire.request("https://fc-ios-test.herokuapp.com/auctions").responseData { response in
            if let json = response.result.value, let auctions = try? JSONDecoder().decode(DataTaskResult<[Auction]>.self, from: json).items {
                completion(auctions)
            }
        }
    }
}
