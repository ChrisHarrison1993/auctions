//
//  DataTaskResult.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Foundation

class DataTaskResult<T: Decodable>: Decodable {
    
    private enum ResultCodingKeys: String, CodingKey {
        case items
    }
    
    let items: T
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ResultCodingKeys.self)
        self.items = try container.decode(T.self, forKey: .items)
    }
}
