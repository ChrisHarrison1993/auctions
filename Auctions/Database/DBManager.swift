//
//  DBManager.swift
//  Auctions
//
//  Created by Christopher Harrison on 24/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    private var database: Realm
    static let shared = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func allAuctions() -> Results<Auction> {
        return database.objects(Auction.self).sorted(byKeyPath: "closeTime", ascending: true)
    }
    
    func save(_ auctions: [Auction]) {
        let auctionList = List<Auction>()
        auctions.forEach({ auctionList.append($0)})
        try! database.write {
            database.add(auctionList, update: true)
        }
    }
}
