//
//  DateConverterTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class DateConverterTests: XCTestCase {

    override func setUp() {
    }

    func test_DateFromStringWithISO8601FormatIsNotNil() {
        let testString = "2019-04-29T22:40:36.901Z"
        let date = try? DateConverter.toDate(dateString: testString, format: .ISO8601)
        XCTAssertNotNil(date)
    }
    
    func test_DateFromStringWithISO8601FormatMissingMillisecondsThrowsError() {
        let testString = "2019-04-29T22:40:36Z"
        
        XCTAssertThrowsError(try DateConverter.toDate(dateString: testString, format: .ISO8601)) { error in
            XCTAssertEqual((error as! DateConversionError).localizedDescription, DateConversionError.failedConvertingFromString(testString).localizedDescription)
        }
        
    }
    
    func test_StringFromDateToISO8601IsNotNil() {
        let date = Date()
        let dateString = DateConverter.toString(date: date, format: .ISO8601)
        XCTAssertNotNil(dateString)
    }

}
