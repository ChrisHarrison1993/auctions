//
//  UILabelExtensionsTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class UILabelExtensionsTests: XCTestCase {

    func test_LabelTextIsNilWhenStyledWithNilStyledText() {
        let label = UILabel()
        label.text = "Test text"
        label.style(with: nil)
        XCTAssertNil(label.text)
    }
    
    func test_LabelTextIsConfiguredByStyledText() {
        let label = UILabel()
        label.style(with: StyledText(text: "Test text", style: .body))
        XCTAssertEqual(label.text, "Test text")
    }
    
    func test_LabelFontIsConfiguredByStyledText() {
        let label = UILabel()
        label.style(with: StyledText(text: "Test text", style: .body))
        XCTAssertEqual(label.font, UIFont.preferredFont(forTextStyle: .body))
    }
    
    func test_LabeltextColorIsConfiguredByStyledText() {
        let label = UILabel()
        label.style(with: StyledText(text: "Test text", style: .body, color: .red))
        XCTAssertEqual(label.textColor, .red)
    }
    
    func test_LabelTextAlignmentIsConfiguredByStyledText() {
        let label = UILabel()
        label.style(with: StyledText(text: "Test text", style: .body, alignment: .right))
        XCTAssertEqual(label.textAlignment, .right)
    }

}
