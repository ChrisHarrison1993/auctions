//
//  NSObjectExtensionsTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class NSObjectExtensionsTests: XCTestCase {

    func test_ClassNameFromAuctionCell() {
        let auctionCell = AuctionCell()
        XCTAssertEqual(auctionCell.className, "AuctionCell")
    }
    
    func test_ClassNameFromSelf() {
        XCTAssertEqual(self.className, "NSObjectExtensionsTests")
    }

}
