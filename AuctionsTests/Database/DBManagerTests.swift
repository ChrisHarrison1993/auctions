//
//  DBManagerTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class DBManagerTests: XCTestCase {

    override func setUp() {
        let testBundle = Bundle(for: type(of: self))
        let mockJSONURL = testBundle.url(forResource: "testAuctions", withExtension: "json")!
        let testAuctions = try! Data(contentsOf: mockJSONURL)
        let auctions = try! JSONDecoder().decode(DataTaskResult<[Auction]>.self, from: testAuctions).items
        DBManager.shared.save(auctions)
    }
    
    func test_AuctionsSavedToDatabase() {
        XCTAssert(DBManager.shared.allAuctions().count > 0)
    }

}
