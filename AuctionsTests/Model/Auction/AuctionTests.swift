//
//  AuctionTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class AuctionTests: XCTestCase {
    
    var auctions: [Auction]!
    
    var testAuctions: Data!
    
    override func setUp() {
        let testBundle = Bundle(for: type(of: self))
        let mockJSONURL = testBundle.url(forResource: "testAuctions", withExtension: "json")!
        testAuctions = try! Data(contentsOf: mockJSONURL)
        auctions = try! JSONDecoder().decode(DataTaskResult<[Auction]>.self, from: testAuctions).items
    }
    
    func test_AllTestAuctionsDecoded() {
        XCTAssertEqual(auctions.count, 12)
    }
    
    func test_AuctionHasPrimaryKey() {
        XCTAssertNotNil(Auction.primaryKey())
    }
}
