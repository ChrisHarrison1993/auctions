//
//  AuctionServiceTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class AuctionServiceTests: XCTestCase {

    func test_AuctionsArrayReturnedFromRequest() {
        var testAuctions: [Auction]!
        let expectation = self.expectation(description: "Auctions")
        AuctionService.shared.getAllAuctions { (auctions) in
            testAuctions = auctions
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertNotNil(testAuctions)
    }
}
