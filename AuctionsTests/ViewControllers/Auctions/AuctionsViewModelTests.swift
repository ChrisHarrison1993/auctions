//
//  AuctionsViewModel.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class AuctionsViewModelTests: XCTestCase {

    var viewModel: AuctionsViewModel!
    var auctions: [Auction]!
    
    var testAuctions: Data!
    
    override func setUp() {
        viewModel = AuctionsViewModel()
        let testBundle = Bundle(for: type(of: self))
        let mockJSONURL = testBundle.url(forResource: "testAuctions", withExtension: "json")!
        testAuctions = try! Data(contentsOf: mockJSONURL)
        auctions = try! JSONDecoder().decode(DataTaskResult<[Auction]>.self, from: testAuctions).items
        DBManager.shared.save(auctions)
    }
    
    func test_ViewModelHasAuctions() {
        XCTAssert(viewModel.auctions.count > 0)
    }
    
    func test_ViewModelHasAuctionFields() {
        XCTAssert(viewModel.auctionFields.count > 0)
    }
    
    func test_AuctionFieldCountIsEqualToAuctionCount() {
        XCTAssertEqual(viewModel.auctions.count, viewModel.auctionFields.count)
    }
    
    func test_SwitchShouldShowEstimatedReturn() {
        let indexPath = IndexPath(row: 0, section: 0)
        let shouldShowEstimatedReturn = viewModel.auctionFields[indexPath.row].showEstimatedReturn
        viewModel.switchShowEstimatedReturnState(at: indexPath)
        let switchedShouldShowEstimatedReturn = viewModel.auctionFields[indexPath.row].showEstimatedReturn
        
        XCTAssertEqual(shouldShowEstimatedReturn, !switchedShouldShowEstimatedReturn)
    }
}
