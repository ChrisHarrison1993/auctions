//
//  AuctionsDataProviderTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class AuctionsDataProviderTests: XCTestCase {
    
    var navVc: UINavigationController!
    var vc: AuctionsViewController!
    var dataProvider: AuctionsDataProvider!

    override func setUp() {
        let storyboard = UIStoryboard(name: "Auctions", bundle: nil)
        navVc = (storyboard.instantiateInitialViewController() as! UINavigationController)
        vc = (navVc.viewControllers.first as! AuctionsViewController)
        _ = vc.view
        dataProvider = vc.dataProvider
    }

    func test_DataProviderHasTableView() {
        XCTAssertNotNil(dataProvider.tableView)
    }
    
    func test_DataProviderHasViewModel() {
        XCTAssertNotNil(dataProvider.viewModel)
    }
    
    func test_TableViewHasOneSection() {
        XCTAssert(dataProvider.tableView.numberOfSections == 1)
    }
}
