//
//  AuctionsViewControllerTests.swift
//  AuctionsTests
//
//  Created by Harrison, Chris (UK - London) on 26/03/2019.
//  Copyright © 2019 Christopher Harrison. All rights reserved.
//

import XCTest

@testable import Auctions
class AuctionsViewControllerTests: XCTestCase {
    
    var navVc: UINavigationController!
    var vc: AuctionsViewController!

    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Auctions", bundle: nil)
        navVc = (storyboard.instantiateInitialViewController() as! UINavigationController)
        vc = (navVc.viewControllers.first as! AuctionsViewController)
        _ = vc.view
    }
    
    func test_HasNavigationController() {
        XCTAssertNotNil(navVc)
    }
    
    func test_HasDataProvider() {
        XCTAssertNotNil(vc.dataProvider)
    }
}
